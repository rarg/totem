<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AccessCodeIsValid
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
      if (!$request->session()->has('access')) {
        return redirect('/')->with('status', 'Introduce tu código de acceso');
      }
      
      return $next($request);
    }
}
