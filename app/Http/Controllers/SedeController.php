<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SedeController extends Controller
{
    public function acceder(Request $request) {
      $code = $request->input('password');
      if($code == env('ACCESS_CODE')){
        $request->session()->put('access', true);
        return redirect('mapa');
      }else{
        return redirect('/')->with('status', 'Código de acceso incorrecto');
      }
    }
}
