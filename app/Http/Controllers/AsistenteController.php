<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Asistente;
use RealRashid\SweetAlert\Facades\Alert;


class AsistenteController extends Controller
{
  public function index($id) {
    $asistentes = Asistente::where('sede_id', '=', $id)->orderBy('name', 'ASC')->get();

    return view('asistentes', ['asistentes' => $asistentes]);
  }

  public function checkin($id, Request $request){

    $success = false;

      try {
        $asistente = Asistente::find($id);
        if(!$asistente){
          $message = 'Asistente no encontrado';
        }else{
          $checkin = $request->checkin;
          $asistente->checkin = $checkin;
          $asistente->save();
          $success = true;
          if($checkin){
            $message = 'Check-in registrado';
          }else{
            $message = 'Check-in eliminado';
          }
        }
      } catch (\Illuminate\Database\QueryException $ex) {
        $success = false;
        $message = $ex->getMessage();
      }

      // response
      $response = [
          'success' => $success,
          'message' => $message,
      ];
      return response()->json($response);

  }
}
