<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SedeController;
use App\Http\Controllers\AsistenteController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('mapa', function () {
  return view('mapa');
})->middleware('accessCode')->name('mapa');

Route::get('sede/{id}', [AsistenteController::class, 'index'] )->middleware('accessCode')->name('asistentes');
Route::post('asistente/{id}', [AsistenteController::class, 'checkin'])->middleware('accessCode')->name('checkin');


Route::post('acceder', [SedeController::class, 'acceder'] )->name('acceder');

