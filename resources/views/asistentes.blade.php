@extends("base")


@section('content')
  <div class="container-fluid bg-totem">
    <div class="container">
      <div class="row justify-content-center text-center">
        <div class="col-12 pt-caminos pb-5">
          <img src="/images/logo_evento.png" class="img-fluid" alt="Logo">
        </div>
        <div class="col-7 py-5">
          <label for="name" class="text-center text-gray pb-2">Encuentra tu nombre en el siguiente listado o bien utiliza el buscador para así confirmar tu asistencia y realizar el check-in:</label><br>
          <input type="text" class="input input-asistente" id="name" name="name"/>
        </div>
      </div>
      <div class="row justify-content-center">
        <div class="col-7 text-gray pb-5 mh-600 overflow-y-scroll" id="lista-asistentes">
          @foreach ($asistentes as $asistente)
          <div class="form-check text-left asistente">
            <div class="row">
              <div class="col-1">
                <input class="check-input" type="checkbox" value="{{ $asistente->id }}" id="{{ $asistente->id }}" {{ ($asistente->checkin == 1) ? 'checked' : '' }}>
              </div>
              <div class="col-11">
                <label class="text-uppercase fs-13rem align-bottom mx-3" for="{{ $asistente->id }}" >
                  {{ $asistente->name }} {{ $asistente->surname }}
                </label>
              </div>
            </div>
          </div>
          @endforeach
        </div> 
      </div>
    </div>
      
    <div class="col-10 py-5 mx-auto">
      <div class="simple-keyboard"></div> 
    </div> 

    <div class="py-3 justify-content-center text-center">
      <button class="btn btn-caminos text-uppercase px-5 py-2"><a href="/mapa" class="text-gray enlace">Volver</a></button>
    </div>
  </div> 

  <script src="/js/keyboard.js"></script
@endsection
