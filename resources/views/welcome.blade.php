@extends("base")


@section('content')
  <div class="container-fluid bg-totem">
    <div class="row justify-content-center text-center">
      <div class="col-12 pt-caminos pb-5">
        <img src="/images/logo_evento.png" class="img-fluid" alt="Logo">
      </div>
      <div class="col-12 py-5">
        <form method="POST" action="/acceder">
          @csrf
          <div>
            <label for="password" class="text-uppercase text-center text-gray pb-2">Contraseña de acceso</label><br>
            <input type="password" class="input input-home" id="password" name="password"/>
          </div>
          <div class="py-3">
            <button type="submit" class="btn btn-caminos text-uppercase px-5 py-2">Acceder</button>
          </div>
          @if (session('status'))
            <div class="col-12 text-gray text-center">
              {{ session('status') }}
            </div>              
          @endif
        </form>
      </div>
    </div>
      
    <div class="col-10 mx-auto">
      <div class="simple-keyboard"></div> 
    </div> 
  </div> 

  <script src="/js/keyboard.js"></script
@endsection
