@extends("base")


@section('content')
  <div class="container-fluid bg-totem">
    <div class="container">
      <div class="row justify-content-center text-center">
        <div class="col-12 pt-caminos pb-5">
          <img src="/images/logo_evento.png" class="img-fluid" alt="Logo">
        </div>
        <div class="col-8 py-5">
          <h3 class="text-center text-gray">Pulsa sobre la sede donde te encuentres para acceder al listado de pasajeros:</h3>
        </div>
        <div class="col-10 py-5">
          <img src="/images/mapa_webapp.png" class="img-fluid" alt="Mapa">
          <div id="santiago" class="sede santiago" data-id="1"></div>
          <div id="bilbao" class="sede bilbao" data-id="2"></div>
          <div id="barcelona" class="sede barcelona" data-id="3"></div>
          <div id="madrid" class="sede madrid" data-id="4"></div>
          <div id="valencia" class="sede valencia" data-id="5"></div>
          <div id="elche" class="sede elche" data-id="6"></div>
          <div id="sevilla" class="sede sevilla" data-id="7"></div>
          <div id="malaga" class="sede malaga" data-id="8"></div>
        </div>
      </div>
    </div>
  </div> 
@endsection

