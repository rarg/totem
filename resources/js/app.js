require('./bootstrap');

const Swal = require('sweetalert2');

$(document).ready(function () {
  const sedes = document.querySelectorAll('.sede');
  if (sedes) {
    sedes.forEach(function (element) {
      element.addEventListener('click', function () {
        window.location = '/sede/' + element.dataset.id;
      });
    });

    sedes.forEach(function (element) {
      element.addEventListener('touchend', function () {
        window.location = '/sede/' + element.dataset.id;
      });
    });
  }

  const checks = document.querySelectorAll('.check-input');
  if (checks) {
    checks.forEach(function (element) {
      element.addEventListener('change', function () {
        axios
          .post("/asistente/" + element.value, { checkin: element.checked })
          .then((response) => {
            if (response.data.success) {
              Swal.fire({
                title: 'CHECK-IN',
                text: response.data.message,
                icon: 'succes',
                confirmButtonText: 'Cool'
              })
            } else {
            }
          })
          .catch(function (error) {
            console.error(error);
          });
      });
    });
  }

});

