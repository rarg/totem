import Keyboard from 'simple-keyboard';
import 'simple-keyboard/build/css/index.css';

const keyboard = new Keyboard({
  onChange: input => onChange(input),
  onKeyPress: button => onKeyPress(button)
});

function onChange(input) {
  document.querySelector(".input").value = input;
}

function onKeyPress(button) {
  if (window.location.pathname.includes('sede')) {
    var input, filter, listaAsistentes, asistentes, div, label, i, txtValue;
    input = document.getElementById("name");
    filter = input.value.toUpperCase();
    listaAsistentes = document.getElementById("lista-asistentes");
    asistentes = listaAsistentes.getElementsByClassName("asistente");
    for (i = 0; i < asistentes.length; i++) {
      label = asistentes[i].getElementsByTagName("label")[0];
      txtValue = label.textContent || label.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        asistentes[i].style.display = "";
      } else {
        asistentes[i].style.display = "none";
      }
    }
  }
}
